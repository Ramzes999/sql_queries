WITH StaffRevenue2017 AS (
  SELECT
    s.store_id,
    s.staff_id,
    CONCAT(s.first_name, ' ', s.last_name) AS staff_name,
    SUM(p.amount) AS total_revenue
  FROM
    staff s
    JOIN payment p ON s.staff_id = p.staff_id
    JOIN rental r ON p.rental_id = r.rental_id
  WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
  GROUP BY
    s.store_id, s.staff_id, staff_name
),
RankedStaffRevenue AS (
  SELECT
    store_id,
    staff_id,
    staff_name,
    total_revenue,
    RANK() OVER (PARTITION BY store_id ORDER BY total_revenue DESC) AS revenue_rank
  FROM
    StaffRevenue2017
)
SELECT
  store_id,
  staff_id,
  staff_name,
  total_revenue
FROM
  RankedStaffRevenue
WHERE
  revenue_rank = 1;

SELECT
  store_id,
  staff_id,
  staff_name,
  total_revenue
FROM (
  SELECT
    s.store_id,
    s.staff_id,
    CONCAT(s.first_name, ' ', s.last_name) AS staff_name,
    SUM(p.amount) AS total_revenue,
    RANK() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
  FROM
    staff s
    JOIN payment p ON s.staff_id = p.staff_id
    JOIN rental r ON p.rental_id = r.rental_id
  WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
  GROUP BY
    s.store_id, s.staff_id, staff_name
) AS RankedStaffRevenue
WHERE
  revenue_rank = 1;

--2nd task
SELECT
    f.title AS movie_title,
    COUNT(r.rental_id) AS rental_count
FROM
    film AS f
JOIN
    inventory AS i
    ON f.film_id = i.film_id
JOIN
    rental AS r
    ON i.inventory_id = r.inventory_id
GROUP BY
    f.title
ORDER BY
    rental_count DESC
LIMIT 5;

SELECT
    movie_title,
    rental_count
FROM (
    SELECT
        f.title AS movie_title,
        COUNT(r.rental_id) AS rental_count,
        ROW_NUMBER() OVER (ORDER BY COUNT(r.rental_id) DESC) AS row_num
    FROM
        film AS f
    JOIN
        inventory AS i
        ON f.film_id = i.film_id
    JOIN
        rental AS r
        ON i.inventory_id = r.inventory_id
    GROUP BY
        f.title
) AS RankedMovies
WHERE
    row_num <= 5;
    
--3rd task   
   
WITH ActorActivity AS (
    SELECT
        actor.actor_id,
        actor.first_name,
        actor.last_name,
        MIN(film.release_year) AS first_movie_year,
        MAX(film.release_year) AS last_movie_year
    FROM
        public.actor AS actor
    JOIN public.film_actor AS film_actor ON actor.actor_id = film_actor.actor_id
    JOIN public.film AS film ON film_actor.film_id = film.film_id
    GROUP BY
        actor.actor_id, actor.first_name, actor.last_name
)
SELECT
    aa.first_name,
    aa.last_name,
    (SELECT EXTRACT(YEAR FROM CURRENT_DATE) - MAX(aa.last_movie_year)) AS years_inactive
FROM
    ActorActivity AS aa
GROUP BY
    aa.first_name, aa.last_name
ORDER BY
    years_inactive DESC;
    
SELECT
    actor.first_name,
    actor.last_name,
    (EXTRACT(YEAR FROM CURRENT_DATE) - aa.last_movie_year) AS years_inactive
FROM (
    SELECT
        actor_id,
        MAX(film.release_year) AS last_movie_year
    FROM
        public.film_actor AS film_actor
    JOIN public.film AS film ON film_actor.film_id = film.film_id
    GROUP BY
        actor_id
) AS aa
JOIN public.actor AS actor ON aa.actor_id = actor.actor_id
ORDER BY
    years_inactive DESC;
   